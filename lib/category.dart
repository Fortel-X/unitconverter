import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

final _rowHeight = 100.0;
final _borderRadius = BorderRadius.all(Radius.circular(50.0));

class Category extends StatelessWidget {
  final _padding = 8.0;
  final _iconSize = 40.0;
  final _iconPadding = 16.0;
  final _textSize = 24.0;
  final String name;
  final Color categoryColor;
  final Color textColor;
  final IconData icon;

  const Category(
      {@required this.name,
        @required this.categoryColor,
        @required this.textColor,
        @required this.icon})
      : assert(name != null),
        assert(categoryColor != null),
        assert(textColor != null),
        assert(icon != null);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: _borderRadius,
        highlightColor: categoryColor,
        splashColor: categoryColor,
        onTap: () => print("I was tapped"),
        child: Container(
          height: _rowHeight,
          decoration: BoxDecoration(
            border: Border.all(
              color: categoryColor,
              width: 1.0,
              style: BorderStyle.solid,
            ),
            borderRadius: _borderRadius,
          ),
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: _iconPadding),
                  child: Icon(
                    icon,
                    size: _iconSize,
                    color: textColor,
                  ),
                ),
                Center(
                  child: Text(
                    name,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: _textSize, color: textColor),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
//        ),
//      ),
    );
  }
}