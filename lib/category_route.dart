import 'package:flutter/material.dart';
import 'package:unit_converter/category.dart';

class CategoryRoute extends StatelessWidget {
  const CategoryRoute();
  static const _appColorAccent = Color.fromARGB(0xff, 190, 144, 212);
  static const _appColor = Color.fromARGB(0xff, 241, 231, 254);
  static const _appColorDark = Color.fromARGB(0xff, 103, 65, 114);

  static const _categoryNames = <String> [
    'Length',
    'Area',
    'Volume',
    'Mass',
    'Time',
    'Digital Storage',
    'Energy',
    'Currency',
    'Temperature',
  ];

  static const _kFontFam = 'MyFlutterApp';
  static const _kFontPkg = null;

  static const _categoryIcon = <IconData> [
    IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg),
    IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg),
  ];

  @override
  Widget build(BuildContext context) {
    final listView = Container(
      color: _appColor,
      child: ListView(
        children: categoryList(),
      ),
    );

    final appBar = AppBar(
      backgroundColor: _appColorAccent,
      title: Text(
          "Unit Converter",
          style: TextStyle(
            fontSize: 30.0,
            backgroundColor: _appColorAccent,
            color: _appColor,
          ),
        ),
    );

    return Scaffold(
      appBar: appBar,
      body: listView,
    );
  }

  List<Widget> categoryList() {
    var categories = <Widget>[];
    for (num i = 0; i < _categoryNames.length; i++) {
      categories.add(Padding(
          padding: EdgeInsets.all(8.0),
          child: Category(
              name: _categoryNames[i],
              categoryColor: _appColorAccent,
              textColor: _appColorDark,
              icon: _categoryIcon[i])));
    }
    return categories;
  }
}